function [im_new]=average(im,mask,T);

im_new=zeros(size(im));
mid=floor(T/2)+1;

dist=[];
for i=1:T
    for j=1:T
        dist(i,j,1)=sqrt((i-mid).^2+((j-mid).^2));
    end
end
dist(mid,mid)=1;
weight= bsxfun(@rdivide,  1, dist);
weight=(1/sum(unfold(weight)))*weight;

if nargin==2
for i=1:size(im,3)
   im_new(:,:,i) = conv2(im(:,:,i),weight,'same');
end
elseif nargin==3
  for i=1:size(im,3)
      img=im(:,:,i);
      map=mask==0;
      img= regionfill(img,map);
   im_new(:,:,i) = conv2(img,weight,'same');
  end
  im_new=bsxfun(@times,im_new,mask);
end



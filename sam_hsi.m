function y=sam_hsi(a1,b1);
% b1 is the reference spectrum, a1=m*n where m is obersavations and n is
% wavelength variables
A=a1';
B=b1';
[~,N] = size(A);
y= zeros(1,N);
for k=1:N
    tmp = A(:,k);
    bt=B;
  y(k) = acos(dot(tmp, bt)/ (norm(bt) * norm(tmp)));
end
end
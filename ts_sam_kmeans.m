function [map1,spe1]=ts_sam_kmeans(C,mask,w_snv)
Ind=find(mask);
X=[];
m=size(C,2);
sp=[];
for i=1:m
    im=C{1,i};
    im=average(im,mask,5);
    im_uf=unfold(im);
    X{1,i}=im_uf(Ind,:);
    if i>1
        sp=[sp;im_uf(Ind,:)];
    end
end

ref=X{1,1};
k(1,1)=0;
range=[];
for i=2:m
    spe=X{1,i};
    r=zeros(size(ref,1),1);
    for j=1:size(ref,1)
        r(j,1)=sam_hsi(spe(j,:),ref(j,:))-sam_hsi(spe(j,:),w_snv);
    end
    range=[range;r];
      k(i,1)=k(i-1,1)+size(r,1);
end
class=kmeans(range,2);


for i=1:2
    Ind1=find(class==i);
    s=mean(sp(Ind1,:));
    spe1(i,:)=s;
end

map1=[];
for i=1:size(C,2)-1
    map1{1,i}=reshape_mask(class(k(i,1)+1:k(i+1,1),:),mask); 
end


% level=multithresh(range,2);
% class=ones(size(range));
% class(class==1)=2;
% class(range<level(1))=1;
% class(range>=level(2))=3;
% 
% map1=[];
% for i=1:size(C,2)-1
%     map1{1,i}=reshape_mask(class(k(i,1)+1:k(i+1,1),:),mask); 
% end
% 
% [B,~] = bwboundaries(mask);
% figure(2)
% white
% subplot(3,4,1)
% dry=double(mask);
%    imagesc(dry,[0 3])
%     hold on;
%     for h=1:length(B)
%         boundary = B{h};
%         plot(boundary(:,2), boundary(:,1),...
%             'w-', 'LineWidth', 2)
%     end
%     title([num2str(TT{1,1})])
%     c=colorbar
%     colorbar('Ticks',[0,1,2,3],...
%         'TickLabels',{'BG','Dry','Inter','Water'})
% 
% for i=1:size(C,2)-1
%     
%     subplot(3,4,i+1)
%     imagesc(map1{1,i},[0 3])
%     hold on;
%     for h=1:length(B)
%         boundary = B{h};
%         plot(boundary(:,2), boundary(:,1),...
%             'w-', 'LineWidth', 2)
%     end
%     title([num2str(TT{i+1,1})])
%     c=colorbar
%     colorbar('Ticks',[0,1,2,3],...
%         'TickLabels',{'BG','Dry','Inter','Water'})
% end
% 
% 
% figure
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.05 0.25 0.9 0.5]);
% white,
% for j=1:m-1
%     subplot(3,4,j)
%     [spe1]=exs(map1{1,j},C{1,j+1},3);
%     legend on
%     plot(W,spe1,'LineWidth',2)
%     grid on,
%     axis tight
%     xlabel('Wavelength','FontSize',8);
%     ylabel('Reflectance','FontSize',8);
%     title([num2str(TT{j+1,1})])
% end
% 
% end
% 
% function  [spe1]=exs(img,im1_masked,k)
% spe1=zeros(k,size(im1_masked,3));
% for i=1:k
%     Ind1=find(img==i);
%     if ~isempty(Ind1)
%     im_uf=unfold(im1_masked);
%     s=mean(im_uf(Ind1,:));
%     spe1(i,:)=s;
%     end
% end
% end
% 
% 

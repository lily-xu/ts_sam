%
% Name:         TS_SAM.m
%               
% Description:  File data.mat consisits of SNV processed hypercubes of casein film after
% contacting with water; SNV processed pure water spectrum; 
% 
%   Author: Junli Xu, 21/02/2018. 
%   Email:  junli.xu@ucdconnect.ie
% Please cite: Xu, J. L., Gowen, A. A., & Sun, D. W. (2018). 
% Time series hyperspectral chemical imaging (HCI) for investigation 
% of spectral variations associated with water and plasticizers in casein 
% based biopolymers. Journal of Food Engineering, 218, 88-105.


%% Visulize data

% Reflectance image at wavelength
figure,white,
for i=1:9
    subplot(3,3,i)
    imagesc(C{1,i}(:,:,70))
    title(num2str(TT{i,1}));
    axis off
end

% Mean spectrum of each hypercube
Ind=find(mask);

figure,white,
for i=1:9
    subplot(3,3,i)
    im_uf=unfold(C{1,i});
    spe=mean(im_uf(Ind,:));
    plot(W,spe,'LineWidth',2);
    grid on,axis tight,
    xlabel('Wavelength');
    ylabel('SNV');
    title(num2str(TT{i,1}));
end

%% Apply time series SAM
[map,spe]=ts_sam_kmeans(C,mask,w_snv);

figure(1)
white
for i=1:8
    subplot(2,4,i)
    imagesc(map{1,i})
 title(num2str(TT{i+1,1}));
 axis off
end
